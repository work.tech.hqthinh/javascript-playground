# KEY NOTES

## IS (Async - Await) blocking the main thread

### For the code below: "NO".

Explanation: The 3 fetches in getUserData() will be executed in order. However, at the location, where the function is called. The
"await" is not present.

Therefore, the console.log('Hello world') will be executed before the getUserData()

```
async function getUserData() {
  let response1 = await fetch("https://jsonplaceholder.typicode.com/users");
  let response2 = await fetch("https://jsonplaceholder.typicode.com/users");
  let response3 = await fetch("https://jsonplaceholder.typicode.com/users");
  console.log("After all promise is executed");
}
getUserData();
console.log("Hello World");

// THE RESULT
    Console - Hello World
    Console - After all promise is executed
```

### For the code below: "YES"

Explanation: The difference is that. Where the "getUserData" function is called. There is the present of "Await".

```
async function getUserData() {
  let response1 = await fetch("https://jsonplaceholder.typicode.com/users");
  let response2 = await fetch("https://jsonplaceholder.typicode.com/users");
  let response3 = await fetch("https://jsonplaceholder.typicode.com/users");
  console.log("After all promise is executed");
}
await getUserData();
console.log("Hello World");

// THE RESULT
    Console - After all promise is executed
    Console - Hello World
```
